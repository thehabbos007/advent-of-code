defmodule AdventOfCode2018.Day02Test do
  use ExUnit.Case

  import AdventOfCode2018.Day02

  test "part1" do
    input = "aabbccdd"

    assert count_chars(input) == %{
             ?a => 2,
             ?b => 2,
             ?c => 2,
             ?d => 2
           }

    assert checksum(["abcdef", "bababc", "abbcde", "abcccd", "aabcdd", "abcdee", "ababab"]) == 12
  end

  @tag :skip
  test "part2" do
    input = ["abcde", "fghij", "klmno", "pqrst", "fguij", "axcye", "wvxyz"]

    assert closest(input) == "fgij"
  end
end
