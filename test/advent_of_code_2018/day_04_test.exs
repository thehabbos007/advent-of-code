defmodule AdventOfCode2018.Day04Test do
  use ExUnit.Case
  import AdventOfCode2018.Day04

  doctest AdventOfCode2018.Day04

  test "part1" do
    input = "[1518-11-10 23:52] Guard #881 begins shift"
    assert parse_log(input) == {{1518, 11, 10}, 23, 52, {:shift, 881}}

    input = "[1518-11-10 23:52] falls asleep"
    assert parse_log(input) == {{1518, 11, 10}, 23, 52, :down}

    input = "[1518-11-10 23:52] wakes up"
    assert parse_log(input) == {{1518, 11, 10}, 23, 52, :up}

    input = [
      "[1518-11-03 00:29] wakes up",
      "[1518-11-03 00:24] falls asleep",
      "[1518-11-04 00:46] wakes up",
      "[1518-11-05 00:03] Guard #99 begins shift",
      "[1518-11-05 00:55] wakes up",
      "[1518-11-04 00:02] Guard #99 begins shift",
      "[1518-11-03 00:05] Guard #10 begins shift",
      "[1518-11-02 00:40] falls asleep",
      "[1518-11-01 00:05] falls asleep",
      "[1518-11-05 00:45] falls asleep",
      "[1518-11-01 00:25] wakes up",
      "[1518-11-01 00:00] Guard #10 begins shift",
      "[1518-11-01 00:55] wakes up",
      "[1518-11-01 00:30] falls asleep",
      "[1518-11-04 00:36] falls asleep",
      "[1518-11-01 23:58] Guard #99 begins shift",
      "[1518-11-02 00:50] wakes up"
    ]

    assert group_by_id_date(input) == [
             {10, {1518, 11, 1}, [5..24, 30..54]},
             {99, {1518, 11, 1}, [40..49]},
             {10, {1518, 11, 3}, [24..28]},
             {99, {1518, 11, 4}, [36..45]},
             {99, {1518, 11, 5}, [45..54]}
           ]

    input = [
      {10, {1518, 11, 1}, [5..24, 30..54]},
      {99, {1518, 11, 1}, [40..49]},
      {10, {1518, 11, 3}, [24..28]},
      {99, {1518, 11, 4}, [36..45]},
      {99, {1518, 11, 5}, [45..54]}
    ]

    assert sum_asleep_times_by_id(input) == %{
             99 => 30,
             10 => 50
           }

    input = %{
      99 => 30,
      10 => 50
    }

    assert guard_asleep_the_most(input) == 10

    input = [
      {10, {1518, 11, 1}, [5..24, 30..54]},
      {99, {1518, 11, 1}, [40..49]},
      {10, {1518, 11, 3}, [24..28]},
      {99, {1518, 11, 4}, [36..45]},
      {99, {1518, 11, 5}, [45..54]}
    ]

    assert minute_asleep_the_most(input) == {99, 45}
  end

  @tag :skip
  test "part2" do
    input = nil
    result = part2(input)

    assert result
  end
end
