defmodule AdventOfCode2018.Day03Test do
  use ExUnit.Case
  doctest AdventOfCode2018.Day03

  import AdventOfCode2018.Day03

  test "part1" do
    input = "#1 @ 257,829: 10x23"
    assert parse_claim(input) == [1, 257, 829, 10, 23]
  end

  test "grid claiming" do
    grid = [[1, 1, 3, 4, 4], [2, 3, 1, 4, 4], [3, 5, 5, 2, 2]]

    claimed = claimed_points(grid)

    assert claimed[{4, 2}] == [2]
    assert claimed[{4, 4}] == [2, 1]
  end

  test "grid overlapping" do
    grid = [[1, 1, 3, 4, 4], [2, 3, 1, 4, 4], [3, 5, 5, 2, 2]]

    overlapped = overlapped_points(grid)

    assert overlapped |> Enum.sort() == [{4, 4}, {4, 5}, {5, 4}, {5, 5}]
  end

  @tag :skip
  test "part2" do
    grid = [[1, 1, 3, 4, 4], [2, 3, 1, 4, 4], [3, 5, 5, 2, 2]]

    assert non_overlappting_grid(grid) == 3
  end
end
