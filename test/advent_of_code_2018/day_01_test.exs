defmodule AdventOfCode2018.Day01Test do
  use ExUnit.Case

  import AdventOfCode2018.Day01

  test "part1" do
    {:ok, io} =
      StringIO.open("""
      +1
      +2
      +1
      -1
      """)

    assert part1(IO.stream(io, :line)) == 3
  end

  test "part2" do
    assert part2(["+1\n", "-2\n", "+3\n", "+1\n"]) == 2
  end
end
