defmodule Mix.Tasks.D03.P2 do
  use Mix.Task

  import AdventOfCode2018.Day03

  @shortdoc "Day 03 Part 2"
  def run(arg) do
    input = arg

    hd(input)
    |> File.read!()
    |> part2()
    |> IO.inspect(label: "Part 2 Results")
  end
end
