defmodule Mix.Tasks.D01.P2 do
  use Mix.Task

  import AdventOfCode2018.Day01

  @shortdoc "Day 01 Part 2"
  def run(arg) do
    input = arg

    hd(input)
    |> File.stream!([], :line)
    |> part2()
    |> IO.inspect(label: "Part 2 Results")
  end
end
