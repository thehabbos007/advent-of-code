defmodule Mix.Tasks.D02.P2 do
  use Mix.Task

  import AdventOfCode2018.Day02

  @shortdoc "Day 02 Part 2"
  def run(args) do
    input = args

    hd(input)
    |> File.stream!([], :line)
    |> part2()
    |> IO.inspect(label: "Part 2 Results")
  end
end
