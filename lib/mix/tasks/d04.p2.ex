defmodule Mix.Tasks.D04.P2 do
  use Mix.Task

  import AdventOfCode2018.Day04

  @shortdoc "Day 04 Part 2"
  def run(args) do
    input = args

    hd(input)
    |> File.read!()
    |> String.split("\n", trim: true)
    |> part2()
    |> IO.inspect(label: "Part 2 Results")
  end
end
