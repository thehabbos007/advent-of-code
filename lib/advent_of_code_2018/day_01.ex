defmodule AdventOfCode2018.Day01 do
  def part1(file_Stream) do
    file_Stream
    |> Stream.map(fn line ->
      {integer, _rest} = Integer.parse(line)
      integer
    end)
    |> Enum.sum()

    # First proposal worked as well, but Valim's method was so nice too
  end

  # Thanks to José Valim's stream i got this one done too
  def part2(file_Stream) do
    file_Stream
    |> Stream.map(fn line ->
      {integer, _rest} = Integer.parse(line)
      integer
    end)
    |> Stream.cycle()
    |> Enum.reduce_while({0, MapSet.new([0])}, fn x, {curr_freq, seen_freq} ->
      new_freq = curr_freq + x

      if new_freq in seen_freq do
        {:halt, new_freq}
      else
        {:cont, {new_freq, MapSet.put(seen_freq, new_freq)}}
      end
    end)
  end
end
