defmodule Cardinality do
  defstruct data: %{}

  def new do
    %Cardinality{}
  end

  def occuring_most(%Cardinality{data: data}) do
    if data != %{} do
      Enum.max_by(data, fn {_, count} -> count end)
    else
      :error
    end
  end

  defimpl Collectable do
    def into(%Cardinality{data: data}) do
      collector_fun = fn
        data, {:cont, elem} -> Map.update(data, elem, 1, &(&1 + 1))
        data, :done -> %Cardinality{data: data}
        _, :halt -> :ok
      end

      {data, collector_fun}
    end
  end
end

defmodule AdventOfCode2018.Day04 do
  import NimbleParsec

  guard_command =
    ignore(string("Guard #"))
    |> integer(min: 1)
    |> ignore(string(" begins shift"))
    |> unwrap_and_tag(:shift)

  sleep_command = ignore(string("falls asleep")) |> replace(:down)

  wake_command = ignore(string("wakes up")) |> replace(:up)

  defparsecp :parsec_log,
             ignore(string("["))
             |> integer(4)
             |> ignore(string("-"))
             |> integer(2)
             |> ignore(string("-"))
             |> integer(2)
             |> ignore(string(" "))
             |> integer(2)
             |> ignore(string(":"))
             |> integer(2)
             |> ignore(string("] "))
             |> choice([
               guard_command,
               sleep_command,
               wake_command
             ])

  def part1(args) do
    grouped_entries = group_by_id_date(args)

    guard_asleep_the_most =
      grouped_entries
      |> sum_asleep_times_by_id()
      |> guard_asleep_the_most()

    {minute_asleep_the_most, _times} =
      minute_asleep_the_most_by_id(grouped_entries, guard_asleep_the_most)

    guard_asleep_the_most * minute_asleep_the_most
  end

  # "[1518-11-10 23:52] Guard #881 begins shift"
  def parse_log(string) when is_binary(string) do
    {:ok, [year, month, date, hour, min, guard], "", _, _, _} = parsec_log(string)
    {{year, month, date}, hour, min, guard}
  end

  def group_by_id_date(unsorted_logs_as_strings) do
    unsorted_logs_as_strings
    |> Enum.map(&parse_log/1)
    |> Enum.sort()
    |> group_by_id_date([])
  end

  defp group_by_id_date([{date, _hour, _minute, {:shift, id}} | rest], groups) do
    {rest, ranges} = get_asleep_ranges(rest, [])
    group_by_id_date(rest, [{id, date, ranges} | groups])
  end

  defp group_by_id_date([], groups) do
    Enum.reverse(groups)
  end

  defp get_asleep_ranges([{_, _, down_minute, :down}, {_, _, up_minute, :up} | rest], ranges) do
    get_asleep_ranges(rest, [down_minute..(up_minute - 1) | ranges])
  end

  defp get_asleep_ranges(rest, ranges) do
    {rest, Enum.reverse(ranges)}
  end

  def sum_asleep_times_by_id(grouped_entries) do
    Enum.reduce(grouped_entries, %{}, fn {id, _date, ranges}, acc ->
      time_asleep = ranges |> Enum.map(&Enum.count/1) |> Enum.sum()
      Map.update(acc, id, time_asleep, &(&1 + time_asleep))
    end)
  end

  def guard_asleep_the_most(map) do
    {id, _} = Enum.max_by(map, fn {_, time_asleep} -> time_asleep end)
    id
  end

  def minute_asleep_the_most(grouped_entries) do
    {current_id, current_minute, _, _} =
      Enum.reduce(grouped_entries, {0, 0, 0, MapSet.new()}, fn {id, _, _}, acc ->
        {current_id, current_minute, current_count, seen_ids} = acc

        if id in seen_ids do
          acc
        else
          case minute_asleep_the_most_by_id(grouped_entries, id) do
            {minute, count} when count > current_count ->
              {id, minute, count, MapSet.put(seen_ids, id)}

            _ ->
              {current_id, current_minute, current_count, MapSet.put(seen_ids, id)}
          end
        end
      end)

    {current_id, current_minute}
  end

  def minute_asleep_the_most_by_id(grouped_entries, id) do
    frequency_map =
      for {^id, _, ranges} <- grouped_entries,
          range <- ranges,
          minute <- range,
          do: minute,
          into: Cardinality.new()

    Cardinality.occuring_most(frequency_map)
  end

  def part2(args) do
    {id, minute} =
      args
      |> group_by_id_date()
      |> minute_asleep_the_most()

    id * minute
  end
end
