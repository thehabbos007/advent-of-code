defmodule AdventOfCode2018.Day02 do
  def part1(file_Stream) do
    file_Stream
    |> Enum.to_list()
    |> checksum()
  end

  def checksum(list) when is_list(list) do
    {twos, threes} =
      Enum.reduce(list, {0, 0}, fn id, {total_2, total_3} ->
        {two, three} =
          id
          |> count_chars()
          |> get_two_and_three()

        {two + total_2, three + total_3}
      end)

    twos * threes
  end

  def get_two_and_three(chars) when is_map(chars) do
    Enum.reduce(chars, {0, 0}, fn
      {_cpoints, 2}, {_two, three} -> {1, three}
      {_cpoints, 3}, {two, _three} -> {two, 1}
      _, acc -> acc
    end)
  end

  def count_chars(string) when is_binary(string) do
    string
    |> String.to_charlist()
    |> Enum.reduce(%{}, fn cpoint, acc ->
      Map.update(acc, cpoint, 1, &(&1 + 1))
    end)
  end

  def part2(file_Stream) do
    file_Stream
    |> Enum.to_list()
    |> closest()
  end

  def closest(list) when is_list(list) do
    list
    |> Enum.map(&String.to_charlist/1)
    |> closest_chars()
  end

  def closest_chars([head | tail]) do
    Enum.find_value(tail, &char_diff(&1, head)) || closest_chars(tail)
  end

  defp char_diff(char_list1, char_list2) do
    char_list1
    |> Enum.zip(char_list2)
    |> Enum.split_with(fn {cpoint1, cpoint2} -> cpoint1 == cpoint2 end)
    |> case do
      {same, [_singe_different]} ->
        same
        |> Enum.map(fn {cpoint, _} -> cpoint end)
        |> List.to_string()

      {_, _list_different} ->
        nil
    end
  end
end
