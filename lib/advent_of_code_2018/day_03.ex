defmodule AdventOfCode2018.Day03 do
  @type claim :: String.t()
  @type parsed_claim :: list
  @type coordinate :: {pos_integer, pos_integer}
  @type id :: integer

  def part1(args) do
    args
    |> String.split("\n", trim: true)
    |> overlapped_points()
    |> length
  end

  @doc """
  Parse a single claim
  ## Examples
    iex> AdventOfCode2018.Day03.parse_claim("#4 @ 186,421: 20x11")
    [4, 186, 421, 20, 11]
  """
  def parse_claim(string) when is_binary(string) do
    string
    |> String.split(["#", " @ ", ",", ": ", "x"], trim: true)
    |> Enum.map(&String.to_integer/1)
  end

  @spec claimed_points([parsed_claim]) :: %{coordinate => [id]}
  def claimed_points(parsed_claim) do
    Enum.reduce(parsed_claim, %{}, fn [id, left, top, width, height], acc ->
      Enum.reduce((left + 1)..(left + width), acc, fn t_x, acc ->
        Enum.reduce((top + 1)..(top + height), acc, fn t_y, acc ->
          Map.update(acc, {t_x, t_y}, [id], &[id | &1])
        end)
      end)
    end)
  end

  @spec overlapped_points([parsed_claim]) :: [coordinate]
  def overlapped_points(parsed_claims) do
    for {coordinate, [_, _ | _]} <- claimed_points(parsed_claims), do: coordinate
  end

  def part2(args) do
    args
    |> String.split("\n", trim: true)
    |> non_overlappting_grid()
  end

  def non_overlappting_grid(claims) do
    parsed_claims = Enum.map(claims, &parse_claim/1)
    claimed_points = claimed_points(parsed_claims)

    [id, _, _, _, _] =
      Enum.find(parsed_claims, fn [id, left, top, width, height] ->
        Enum.all?((left + 1)..(left + width), fn x ->
          Enum.all?((top + 1)..(top + height), fn y ->
            Map.get(claimed_points, {x, y}) == [id]
          end)
        end)
      end)

    id
  end
end
